/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.iotlib.comm.message;

import java.nio.ByteBuffer;
import javax.websocket.DecodeException;
import javax.websocket.EndpointConfig;

/**
 *
 * @author Arief Prihasanto
 */
public class MessageDecoder implements javax.websocket.Decoder.Binary<Message> {

    @Override
    public Message decode(ByteBuffer bytes) throws DecodeException {
        return new Message(bytes);
    }

    @Override
    public boolean willDecode(ByteBuffer bytes) {
        return true;
    }

    @Override
    public void init(EndpointConfig config) {

    }

    @Override
    public void destroy() {

    }

}
