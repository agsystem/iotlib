/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.iotlib.comm.message;

import java.util.UUID;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public abstract class AbstractMessage<T> {
    
    protected T payload;

    public abstract void setSource(UUID srcId);

    public abstract UUID getSource();
    
    public abstract void setInput(int input);
    
    public abstract int getInput();
    
    public abstract void setOutput(int output);
    
    public abstract int getOutput();

    public abstract void setTimestamp(long timestamp);

    public abstract long getTimestamp();
    
    public abstract void setFlag(int flag);
    
    public abstract int getFlag();

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
    
}
