/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.iotlib.comm.message;

import java.nio.ByteBuffer;
import java.util.UUID;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class Message {

    private final ByteBuffer bytes;

    public Message(int szPayload) {
        bytes = ByteBuffer.allocate(52 + szPayload);
    }

    public Message(ByteBuffer bytes) {
        this.bytes = bytes;
    }

    public void setSource(UUID srcId) {
        bytes.asLongBuffer().put(0, srcId.getMostSignificantBits()).put(1, srcId.getLeastSignificantBits());
    }

    public UUID getSource() {
        bytes.position(0);
        long[] uuid = new long[2];
        bytes.asLongBuffer().get(uuid);
        return new UUID(uuid[0], uuid[1]);
    }

    public void setTarget(UUID srcId) {
        bytes.asLongBuffer().put(2, srcId.getMostSignificantBits()).put(3, srcId.getLeastSignificantBits());
    }

    public UUID getTarget() {
        bytes.position(16);
        long[] uuid = new long[2];
        bytes.asLongBuffer().get(uuid);
        return new UUID(uuid[0], uuid[1]);
    }
    
    public void setInput(int input) {
        bytes.position(32);
        bytes.asIntBuffer().put(input);
    }
    
    public int getInput() {
        bytes.position(32);
        return bytes.asIntBuffer().get();
    }
    
    public void setOutput(int output) {
        bytes.position(36);
        bytes.asIntBuffer().put(output);
    }
    
    public int getOutput() {
        bytes.position(36);
        return bytes.asIntBuffer().get();
    }

    public void setTimestamp(long timestamp) {
        bytes.position(40);
        bytes.asLongBuffer().put(timestamp);
    }

    public long getTimestamp() {
        bytes.position(40);
        return bytes.asLongBuffer().get();
    }
    
    public void setFlag(int flag) {
        bytes.position(48);
        bytes.asIntBuffer().put(flag);
    }
    
    public int getFlag() {
        bytes.position(48);
        return bytes.asIntBuffer().get();
    }
    
    public void putByte(byte val) {
        bytes.position(52);
        bytes.put(val);
    }
    
    public void putBytes(byte[] val) {
        bytes.position(52);
        bytes.put(val);
    }

    public void putShort(short val) {
        bytes.position(52);
        bytes.asShortBuffer().put(val);
    }

    public void putInt(int val) {
        bytes.position(52);
        bytes.asIntBuffer().put(val);
    }

    public void putLong(long val) {
        bytes.position(52);
        bytes.asLongBuffer().put(val);
    }

    public void putFloat(float val) {
        bytes.position(52);
        bytes.asFloatBuffer().put(val);
    }

    public void putDouble(double val) {
        bytes.position(52);
        bytes.asDoubleBuffer().put(val);
    }

    public void putChar(char val) {
        bytes.position(52);
        bytes.asCharBuffer().put(val);
    }

    public void putString(String val) {
        bytes.position(52);
        byte[] b = val.getBytes();
        bytes.put(b);
    }

    public byte getByte() {
        bytes.position(52);
        return bytes.get();
    }

    public int getShort() {
        bytes.position(52);
        return bytes.asShortBuffer().get();
    }

    public int getInt() {
        bytes.position(52);
        return bytes.asIntBuffer().get();
    }

    public long getLong() {
        bytes.position(52);
        return bytes.asLongBuffer().get();
    }

    public double getDouble() {
        bytes.position(52);
        return bytes.asDoubleBuffer().get();
    }

    public float getFloat() {
        bytes.position(52);
        return bytes.asFloatBuffer().get();
    }

    public char getChar() {
        bytes.position(52);
        return bytes.asCharBuffer().get();
    }
    
    public String getString() {
        bytes.position(52);
        bytes.limit(bytes.capacity());
        byte[] result = new byte[bytes.remaining()];
        bytes.get(result);
        
        return new String(result);
    }

    public ByteBuffer getBytes() {

        ByteBuffer flipped = (ByteBuffer) bytes.flip();
        flipped.limit(flipped.capacity());
        
        return bytes;
    }
}
