/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.iotlib.comm.message;

import java.nio.ByteBuffer;
import javax.websocket.EncodeException;
import javax.websocket.EndpointConfig;

/**
 *
 * @author Arief Prihasanto
 */
public class MessageEncoder implements javax.websocket.Encoder.Binary<Message> {

    @Override
    public ByteBuffer encode(Message object) throws EncodeException {
        return object.getBytes();
    }
    

    @Override
    public void init(EndpointConfig config) {
        
    }

    @Override
    public void destroy() {
        
    }
}
