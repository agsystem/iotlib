/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.iotlib.comm.message;

/**
 *
 * @author Arief Prihasanto
 */
public interface MessageListener {
    public void onMessage(Message msg);
}
