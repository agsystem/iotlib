/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.iotlib.comm.message;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public abstract class Jsonable<T> {
    
    protected T data;

    public Jsonable(T data) {
        this.data = data;
    }
    
    public String toJsonData() {
        return jsonify(data);
    }
    
    protected abstract String jsonify(T data);
    
}
