/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.iotlib.comm.message;

import com.google.gson.Gson;
import java.util.Formatter;
import java.util.UUID;
import mdn.iotlib.comm.dtype.IntegerJsonable;
import mdn.iotlib.comm.dtype.LongJsonable;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class JsonMessage<T extends Jsonable> {
    
    private static String jsonTemplate = "{"
            + "sourceHigh:%s,"
            + "sourceLow:%s,"
            + "input:%s,"
            + "output:%s,"
            + "timestamp:%s,"
            + "flag:%s,"
            + "payload:%s"
            + "}";
    
    private long sourceHigh;
    
    private long sourceLow;
    
    private int input;
    
    private int output;
    
    private long timestamp;
    
    private int flag;
    
    private T payload;

    public JsonMessage() {
    }

    public JsonMessage(String json) {
        
    }
    
    public UUID getSource() {
        return new UUID(sourceHigh, sourceLow);
    }
    
    public void setSource(UUID srcId) {
        sourceHigh = srcId.getMostSignificantBits();
        sourceLow = srcId.getLeastSignificantBits();
    }

    private String getJsonSourceHigh() {
        Jsonable jsonable = new LongJsonable(sourceHigh);
        return jsonable.toJsonData();
    }

    private String getJsonSourceLow() {
        Jsonable jsonable = new LongJsonable(sourceLow);
        return jsonable.toJsonData();
    }

    public int getInput() {
        return input;
    }

    public void setInput(int input) {
        this.input = input;
    }
    
    private String getJsonInput(){
        Jsonable jsonable = new IntegerJsonable(input);
        return jsonable.toJsonData();
    } 

    public int getOutput() {
        return output;
    }

    public void setOutput(int output) {
        this.output = output;
    }
    
    private String getJsonOutput(){
        Jsonable jsonable = new IntegerJsonable(output);
        return jsonable.toJsonData();
    } 

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
    
    private String getJsonTimestamp(){
        Jsonable jsonable = new LongJsonable(timestamp);
        return jsonable.toJsonData();
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }
    
    private String getJsonFlag(){
        Jsonable jsonable = new IntegerJsonable(flag);
        return jsonable.toJsonData();
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
    
    private String getJsonPayload(){        
        return payload.toJsonData();
    }
    
    public String getJson() {
        Formatter fmt = new Formatter();
        fmt.format(jsonTemplate, getJsonSourceHigh(), getJsonSourceLow(), getJsonInput(), getJsonOutput(), getJsonTimestamp(), getJsonFlag(), getJsonPayload());
        return fmt.toString();
    }
    
}
