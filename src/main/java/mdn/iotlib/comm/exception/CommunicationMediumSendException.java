/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.iotlib.comm.exception;

/**
 *
 * @author Arief Prihasanto
 */
public class CommunicationMediumSendException extends Exception {

    public CommunicationMediumSendException() {
        super("Send error.");
    }

    public CommunicationMediumSendException(Throwable cause) {
        super("Send error.", cause);
    }
    
}
