/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.iotlib.comm.exception;

/**
 *
 * @author Arief Prihasanto
 */
public class CommunicationMediumConnectException extends Exception {

    public CommunicationMediumConnectException() {
        super("Connection failed.");
    }

    public CommunicationMediumConnectException(Throwable cause) {
        super("Connection failed.", cause);
    }
    
}
