/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.iotlib.comm.dtype;

import mdn.iotlib.comm.message.Jsonable;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class IntegerJsonable extends Jsonable<Integer> {

    public IntegerJsonable(Integer data) {
        super(data);
    }

    @Override
    public String jsonify(Integer data) {
        return Integer.toString(data);
    }
    
}
