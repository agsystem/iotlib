/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdn.iotlib.comm.dtype;

import mdn.iotlib.comm.message.Jsonable;

/**
 *
 * @author Arief Prihasanto <aphasan57 at gmail.com>
 */
public class LongJsonable extends Jsonable<Long> {

    public LongJsonable(Long data) {
        super(data);
    }

    @Override
    public String jsonify(Long data) {
        return Long.toString(0);
    }
    
}
